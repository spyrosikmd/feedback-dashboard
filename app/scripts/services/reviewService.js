'use strict';

/**
 * @ngdoc service
 * @name feedbackDashboard.reviewService
 * @description
 * # reviewService
 * Service in the feedbackDashboard.
 */
angular
  .module('feedbackDashboard')
  .service('reviewService', ['$http', function ($http) {

    this.fetch = function() {

      var config = {
        method: 'GET',
        url: 'http://cache.usabilla.com/example/apidemo.json'
      };

      return $http(config);
    };

  }]);
