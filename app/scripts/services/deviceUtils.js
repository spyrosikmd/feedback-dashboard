'use strict';

/**
 * @ngdoc service
 * @name feedbackDashboard.deviceUtils
 * @description
 * # deviceUtils
 * Service in the feedbackDashboard.
 */
angular
  .module('feedbackDashboard')
  .service('deviceUtils', function () {

    var tabletRegex = /^.*(GT-P(3113|51|75|52|73|10|62|68|71)|GT-N(80|51)|SM-T(31|21)|Nexus (7|10)|B1-A71|Tablet S|SGP(T12|331)|Slate 7|TF(101|300T|700T)|MZ(60|601|605)|A(2107|100|200|500|1-810|3000-F|110)|AT(100|300)|Chagall|ME(17|371MG|301T)|Xoom|MID7048|iPad).*$/,
      mobileRegex = /^.*(Android|iP(hone|od)|Symbian|BlackBerry|Opera M(ini|obi)|IEMobile|GoBrowser|Blazer|BOLT|Fennec|Iris|Maemo|MIB|Minimo|NetFront|SEMC-Browser|Skyfire|TeaShark|Teleca|uZard).*$/;

    this.getDevice = function(userAgent) {
      var device = 'desktop';

      if (isMobile(userAgent)) {
        device = 'mobile';
      } else if(isTablet(userAgent)) {
        device = 'tablet';
      }

      return device;
    };

    function isMobile(userAgent) {
      return userAgent.match(mobileRegex);
    }

    function isTablet(userAgent) {
      return userAgent.match(tabletRegex);
    }

  });

