'use strict';

/**
 * @ngdoc function
 * @name feedbackDashboard.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the feedbackDashboard
 */
angular
  .module('feedbackDashboard')
  .controller('DashboardCtrl', ['$scope', 'reviewService',
    function ($scope, reviewService) {

      reviewService.fetch()
        .then(function(response) {
          $scope.reviews = response.data.items;
          $scope.filteredReviews = _.clone(response.data.items);
        });

        $scope.$on('reviews.filtered', function(event, data) {
          $scope.filteredReviews = data;
        });

        $scope.showExtraCharts = function() {
          $scope.extraCharts = true;
        };

  }]);
