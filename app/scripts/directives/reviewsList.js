'use strict';

/**
 * @ngdoc directive
 * @name feedbackDashboard.directive:reviewsList
 * @description
 * # reviewsList
 */
angular
  .module('feedbackDashboard')
  .directive('reviewsList', ['deviceUtils', function (deviceUtils) {
    return {
      templateUrl: 'views/reviewsList.html',
      restrict: 'AE',
      scope: {
        reviews: '='
      },
      link: function(scope) {

        scope.reviews = _.orderBy(scope.reviews, ['rating']);

        scope.getDevice = function(userAgent) {
          return deviceUtils.getDevice(userAgent);
        };

      }
    };
  }]);

