'use strict';

/**
 * @ngdoc directive
 * @name feedbackDashboard.directive:reviewsFilter
 * @description
 * # reviewsFilter
 */
angular
  .module('feedbackDashboard')
  .directive('reviewsFilter', function () {
    return {
      templateUrl: 'views/reviewsFilter.html',
      restrict: 'AE',
      scope: {
        reviews: '='
      },
      link: function(scope, elem) {

        var originalReviews = _.clone(scope.reviews),
          ratingFilters = [
            {
              rating: 1,
              active: true
            },
            {
              rating: 2,
              active: true
            },
            {
              rating: 3,
              active: true
            },
            {
              rating: 4,
              active: true
            },
            {
              rating: 5,
              active: true
            }
          ];

        scope.updateRatingFilter = function(rating) {
          elem[0].getElementsByClassName('buttonFilter')[rating - 1].classList.toggle('disabled');
          var ratingFilter = _.find(ratingFilters, {'rating': rating});
          ratingFilter.active = !ratingFilter.active;
          applyFilters();
        };

        function applyFilters() {
          var activeFilters = _.filter(ratingFilters, 'active');

          if (activeFilters.length < 1) {
            scope.reviews = [];
          } else {
            filterReviews(activeFilters);
          }
        }

        function filterReviews(activeFilters) {
          scope.reviews = _.filter(originalReviews, function(o) {
            var found = [];
            _.forEach(activeFilters, function(activeFilter) {
              if (o.rating === activeFilter.rating) {
                found.push(o);
              }
            });

            return found.length > 0;
          });
          scope.$emit('reviews.filtered', scope.reviews);
        }

      }
    };
  });

