'use strict';

/**
 * @ngdoc directive
 * @name feedbackDashboard.directive:averageRatingChart
 * @description
 * # averageRatingChart
 */
angular
  .module('feedbackDashboard')
  .directive('averageRatingChart', ['moment', function (moment) {
    return {
      templateUrl: 'views/averageRatingChart.html',
      restrict: 'AE',
      scope: {
        reviews: '='
      },
      link: function(scope) {

          scope.reviews = _.orderBy(scope.reviews, ['creation_date']);

          scope.ratingChart = {
            labels: [],
            data: [],
            options: {
              scaleShowVerticalLines: false,
              datasetFill: false,
              bezierCurve: false
            },
            colours: ['#00A5C9']
          };

          var dataByDate =  {};
          _.forEach(scope.reviews, function(review) {
            var date = moment.unix(review.creation_date).format('DD MMM');
            if (_.indexOf(scope.ratingChart.labels, date) === -1) {
              scope.ratingChart.labels.push(date);
            }

            if (dataByDate[date]) {
              dataByDate[date].push(review.rating);
            } else {
              dataByDate[date] = [review.rating];
            }
          });

          var serie = [];
          _.forEach(scope.ratingChart.labels, function(label) {
            serie.push(_.reduce(dataByDate[label], Math.average));
          });
          scope.ratingChart.data[0] = serie;

      }
    };
  }]);

