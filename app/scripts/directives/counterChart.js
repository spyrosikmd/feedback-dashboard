'use strict';

/**
 * @ngdoc directive
 * @name feedbackDashboard.directive:counterChart
 * @description
 * # counterChart
 */
angular
  .module('feedbackDashboard')
  .directive('counterChart', function () {
    return {
      templateUrl: 'views/counterChart.html',
      restrict: 'AE',
      scope: {
        reviews: '=',
        property: '@'
      },
      link: function(scope) {

          scope.counterChart = {
            labels: [],
            data: [],
            options: {
              scaleShowVerticalLines: false
            },
          };

          var serie = [],
            counter = _.countBy(scope.reviews, scope.property);

          _.forEach(counter, function(count, property) {
            scope.counterChart.labels.push(property);
            serie.push(count);
          });

          scope.counterChart.data[0] = serie;

      }
    };
  });

