'use strict';

/**
 * @ngdoc directive
 * @name feedbackDashboard.directive:ratingsCounter
 * @description
 * # ratingsCounter
 */
angular
  .module('feedbackDashboard')
  .directive('ratingsCounter', function () {
    return {
      templateUrl: 'views/ratingsCounter.html',
      restrict: 'AE',
      scope: {
        reviews: '='
      },
      link: function(scope) {

        scope.ratingsCounter = _.countBy(scope.reviews, 'rating');

        var ratingsLabels = {
          1: 'Very bad',
          2: 'Bad',
          3: 'Average',
          4: 'Good',
          5: 'Amazing!'
        };

        scope.getRatingLabel = function(rating) {
          return ratingsLabels[rating];
        };

      }
    };
  });

