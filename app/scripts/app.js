'use strict';

/**
 * @ngdoc overview
 * @name feedbackDashboard
 * @description
 * # feedbackDashboard
 *
 * Main module of the application.
 */
angular
  .module('feedbackDashboard', [
    'ngRoute',
    'chart.js'
  ])

  .value('moment', window.moment)

  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });


