'use strict';

describe('Controller: DashboardCtrl', function () {

  beforeEach(module('feedbackDashboard'));

  var DashboardCtrl,
    scope,
    $httpBackend,
    $rootScope,
    $controller,
    $q;

  beforeEach(inject(function(_$controller_, _$rootScope_, _$q_, _$httpBackend_) {
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    $rootScope = _$rootScope_;
    $controller = _$controller_;

    var mockReviews = {
      data: {
        'items': [
            {
              'comment':'belle offre de services',
              'computed_browser':{'Browser':'Chrome','Version':'32.0','Platform':'MacOSX','FullBrowser':'Chrome'},
              'computed_location':'France',
              'creation_date':1391445344,
              'rating':5
            }
        ]
      }
    };

    var spy = jasmine.createSpyObj('reviewService', ['fetch']);

    spy.fetch.and.callFake(function() {
      var deferred = $q.defer();
      deferred.resolve(mockReviews);
      return deferred.promise;
    });

    $httpBackend.whenGET('views/dashboard.html')
       .respond('dashboard');

    scope = $rootScope.$new();
    DashboardCtrl = $controller('DashboardCtrl', {
      $scope: scope,
      reviewService: spy
    });
    scope.$digest();

    expect(DashboardCtrl).toBeDefined();
    expect(spy.fetch).toHaveBeenCalled();

    //$httpBackend.flush();
  }));

  it('should attach a list of reviews to the scope', function() {
    expect(scope.reviews.length).toBe(1);
    expect(scope.reviews[0].rating).toEqual(5);
    expect(scope.reviews[0].computed_location).toEqual('France');
  });
});
