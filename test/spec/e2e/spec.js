describe('Feedback Dashboard', function() {

  beforeEach(function() {
    browser.get('http://localhost:9000/');
  });

  it('should have a title', function() {
    expect(browser.getTitle()).toEqual('Feedback Dashboard');
  });

  it('should have a header', function() {
    var header = element(by.css('.page-header'));
    expect(header.isPresent()).toBe(true);
    expect(header.getText()).toBe('Dashboard');
  });

  it('should have a Ratings Counter', function() {
    expect(element(by.css('.ratingsCounter')).isPresent()).toBe(true);
  });

  it('should have an Average Rating Chart', function() {
    expect(element(by.css('.averageRatingChart')).isPresent()).toBe(true);
  });

  it('should have a Show more charts cta', function() {
    expect(element(by.css('.showMoreCharts')).isPresent()).toBe(true);
  });

  it('should have a Search Box', function() {
    expect(element(by.css('.searchBox')).isPresent()).toBe(true);
  });

  it('should have a Review Filter', function() {
    expect(element(by.css('.reviewFilters')).isPresent()).toBe(true);
  });

  it('should have a list of reviews', function() {
    expect(element(by.css('.reviews')).isPresent()).toBe(true);
    var list = element.all(by.css('.reviewDetails'));
    expect(list.count()).toBeGreaterThan(0);
  });

  it('should filter a list of reviews', function() {
    element(by.model('searchBox')).sendKeys('guara guara guara');
    browser.wait(function() {
      return element(by.css('.reviewDetails')).isPresent();
    }).then(function() {
      var list = element.all(by.css('.reviewDetails'));
      expect(list.count()).toBe(1);
    });
  });

  it('should filter a list of reviews until empty', function() {
    element.all(by.css('.buttonFilter')).click();
    var list = element.all(by.css('.reviewDetails'));
    expect(list.count()).toBe(0);
  });

});
