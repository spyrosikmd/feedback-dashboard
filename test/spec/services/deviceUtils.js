'use strict';

describe('Service: deviceUtils', function () {

  beforeEach(module('feedbackDashboard'));

  var deviceUtils;

  beforeEach(inject(function(_deviceUtils_) {
    deviceUtils = _deviceUtils_;
  }));

  it('should detect the device from the user agent', function() {

    var desktopUA = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36';
    var mobileUA = 'Mozilla/5.0 (Linux; U; Android 4.2.2; pl-pl; C6603 Build/10.3.1.A.2.67) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30';
    var tabletUA = 'Mozilla/5.0 (iPad; CPU OS 7_0_4 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B554a Safari/9537.53';

    expect(deviceUtils.getDevice(desktopUA)).toBe('desktop');
    expect(deviceUtils.getDevice(tabletUA)).toBe('tablet');
    expect(deviceUtils.getDevice(mobileUA)).toBe('mobile');

  });

});
