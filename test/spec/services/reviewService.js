'use strict';

describe('Service: reviewService', function () {

  beforeEach(module('feedbackDashboard'));

  var reviewService,
    $httpBackend;

  beforeEach(inject(function(_$httpBackend_, _reviewService_) {
    reviewService = _reviewService_;
    $httpBackend = _$httpBackend_;
  }));

  it('should get a list of reviews', function() {

    var mockResponse = {
      items: [
        {
          creation_date: 1391445344,
          rating: 5,
          comment: 'belle offre de services'
        },
        {
          creation_date: 1391445293,
          rating: 2,
          comment: 'bouton ne fonctionne pas'
        }
      ]
    };

    $httpBackend.when('GET', 'http://cache.usabilla.com/example/apidemo.json')
      .respond(mockResponse);

    reviewService.fetch()
      .then(function(response) {
        expect(response).toBeDefined();
        expect(response.data.items.length).toBeGreaterThan(0);
        expect(response.data.items[0].rating).toBe(5);
      });

    $httpBackend.whenGET('views/dashboard.html')
       .respond('dashboard');

    $httpBackend.flush();
  });

});
