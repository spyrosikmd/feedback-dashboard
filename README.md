# Feedback Dashboard

## Requirements

### Node
https://nodejs.org/en/download/

### Grunt
http://gruntjs.com/installing-grunt

`sudo npm install -g grunt-cli`

### Bower
http://bower.io/

`sudo npm install -g bower`

### Ruby
https://www.ruby-lang.org/en/downloads/

### Compass
http://compass-style.org/install/

`gem install compass`

## Initial setup

### Clone the repo from bitbucket

`https://bitbucket.org/pabloosso/feedback-dashboard`

### Change directory to language-selector

`cd feedback-dashboard`

### Install npm dependencies

`npm install`

### Install bower dependencies

`bower install`

## Build & development

Run `grunt build` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

### End to End Tests

### Install protractor
http://angular.github.io/protractor

`npm install -g protractor`

### Start the webdriver (needs Java)

`webdriver-manager start`

### Run the tests

`protractor protractor.js`

